package com.ccteam.lostandfound;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LostAndFoundServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LostAndFoundServerApplication.class, args);
    }

}
